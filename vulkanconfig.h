#ifndef VULKANCONFIG_H
#define VULKANCONFIG_H

#include <QByteArrayList>
#include <vulkan/vulkan.h>

#ifdef NDEBUG
    const bool enableValidationLayers = false;
#else
    const bool enableValidationLayers = true;
#endif
    const QByteArrayList validationLayers = (QByteArrayList() << "VK_LAYER_LUNARG_standard_validation");

const std::vector<const char*> deviceExtensions = {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

const int MAX_FRAMES_IN_FLIGHT = 2;

#endif // VULKANCONFIG_H
