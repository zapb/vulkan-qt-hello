#ifndef VULKANWINDOW_H
#define VULKANWINDOW_H

#include <QWindow>

#include "vulkanutils.h"

#include <vulkan/vulkan.h>

#include <optional>

class AbstractScene;

struct QueueFamilyIndices
{
    std::optional<uint32_t> graphicsFamily;
    std::optional<uint32_t> presentFamily;

    bool isComplete() const
    {
        return graphicsFamily.has_value() && presentFamily.has_value();
    }
};

struct SwapChainSupportDetails
{
    VkSurfaceCapabilitiesKHR capabilities;
    std::vector<VkSurfaceFormatKHR> formats;
    std::vector<VkPresentModeKHR> presentModes;
};

class VulkanWindow : public QWindow
{
public:
    VulkanWindow(QScreen *screen = nullptr);
    ~VulkanWindow();

    void setScene(AbstractScene *scene);
    const AbstractScene *scene() const { return m_scene; }
    AbstractScene *scene() { return m_scene; }

    QueueFamilyIndices queueFamilyIndices(VkPhysicalDevice device) const;
    VkPhysicalDevice physicalDevice() const { return m_physicalDevice; }
    VkDevice device() const { return m_device; }
    VkExtent2D swapChainExtent() const { return m_swapChainExtent; }
    size_t swapChainImageCount() const { return m_swapChainImages.size(); }
    uint32_t currentSwapChainImageIndex() const { return m_currentSwapChainImageIndex; }
    size_t currentFrameIndex() const { return m_currentFrame; }
    VkFramebuffer swapChainFramebuffer(size_t i) const { return m_swapChainFramebuffers[i]; }
    VkSemaphore renderFinishedSemaphore(size_t i) const { return m_renderFinishedSemaphores[i]; }
    VkSemaphore imageAvailableSemaphore(size_t i) const { return m_imageAvailableSemaphores[i]; }
    VkFence inFlightFence(size_t i) const { return m_inFlightFences[i]; }
    VkRenderPass renderPass() const { return m_renderPass; }
    VkQueue graphicsQueue() const { return m_graphicsQueue; }

protected:
    void exposeEvent(QExposeEvent *e);
    void resizeEvent(QResizeEvent *e);
    bool event(QEvent *e);
    void render();

private:
    void initVulkan();

    void cleanup();
    void cleanupSwapChain();
    void recreateSwapChain();

    void pickPhysicalDevice();
    bool isDeviceSuitable(VkPhysicalDevice device);
    bool checkDeviceExtensionSupport(VkPhysicalDevice device);
    SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device);

    void createLogicalDevice();
    void createSwapChain();
    VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR> &availableFormats);
    VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR> &availablePresentModes);
    VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR &capabilities);
    void createSwapChainImageViews();

    void createRenderPass();
    VkFormat findSupportedFormat(const std::vector<VkFormat> &candidates,
                                 VkImageTiling tiling,
                                 VkFormatFeatureFlags features);
    VkFormat findDepthFormat();
    void createDepthResources();
    void transitionImageLayout(VkImage image, VkFormat format,
                               VkImageLayout oldLayout, VkImageLayout newLayout);
    void createFramebuffers();

    void createCommandPool();
    void createSyncObjects();

    VkCommandBuffer beginSingleShotCommands()
    {
        return beginSingleShotCommandsHelper(m_device, m_commandPool);
    }

    void endSingleShotCommands(VkCommandBuffer commandBuffer)
    {
        endSingleShotCommandsHelper(m_device, m_graphicsQueue, m_commandPool, commandBuffer);
    }

    AbstractScene *m_scene;
    bool m_initialized;
    bool m_framebufferResized;

    // Top-level
    VkInstance m_instance;
    VkSurfaceKHR m_surface;
    VkPhysicalDevice m_physicalDevice;

    // Device and queues
    VkDevice m_device;
    VkQueue m_graphicsQueue;
    VkQueue m_presentQueue;

    // Swapchain
    VkSwapchainKHR m_swapChain;
    std::vector<VkImage> m_swapChainImages;
    VkFormat m_swapChainImageFormat;
    VkExtent2D m_swapChainExtent;
    std::vector<VkImageView> m_swapChainImageViews;
    VkImage m_depthImage;
    VkDeviceMemory m_depthImageMemory;
    VkImageView m_depthImageView;
    VkRenderPass m_renderPass;
    std::vector<VkFramebuffer> m_swapChainFramebuffers;

    // Helpers
    VkCommandPool m_commandPool;

    // Synchronisation
    std::vector<VkSemaphore> m_imageAvailableSemaphores; // GPU queue - GPU queue sync
    std::vector<VkSemaphore> m_renderFinishedSemaphores; // GPU queue - GPU queue sync
    std::vector<VkFence> m_inFlightFences; // GPU - CPU sync
    size_t m_currentFrame;
    uint32_t m_currentSwapChainImageIndex;
};

#endif // VULKANWINDOW_H
