#include <QDebug>
#include <QGuiApplication>
#include <QVulkanInstance>

#include "vulkanwindow.h"
#include "vulkanconfig.h"
#include "simplescene.h"

int main(int argc, char *argv[])
{
    QGuiApplication a(argc, argv);

    QVulkanInstance instance;

    if (enableValidationLayers) {
        // Enable validation layer, if supported. Messages go to qDebug by default.
        instance.setLayers(validationLayers);
    }

    bool ok = instance.create();
    if (!ok) {
        qCritical() << "Vulkan support not available";
        return 1;
    }

    if (enableValidationLayers && !instance.layers().contains("VK_LAYER_LUNARG_standard_validation")) {
        qCritical() << "Validation layer support not available";
        return 1;
    }

    VulkanWindow w;
    w.setVulkanInstance(&instance);

    SimpleScene scene;
    w.setScene(&scene);

    w.resize(800, 600);
    w.show();

    return a.exec();
}
