#ifndef VULKANUTILS_H
#define VULKANUTILS_H

#include <vulkan/vulkan.h>

uint32_t findMemoryType(VkPhysicalDevice physicalDevice,
                        uint32_t typeFilter,
                        VkMemoryPropertyFlags properties);

void createImage(VkPhysicalDevice physicalDevice, VkDevice device,
                 uint32_t width, uint32_t height,
                 VkFormat format, VkImageTiling tiling,
                 VkImageUsageFlags usage, VkMemoryPropertyFlags memoryProperties,
                 VkImage &image, VkDeviceMemory &imageMemory);

VkImageView createImageView(VkDevice device,
                            VkImage image,
                            VkFormat format,
                            VkImageAspectFlags aspectFlags);

inline bool hasStencilComponent(VkFormat format)
{
    return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
}

VkCommandBuffer beginSingleShotCommandsHelper(VkDevice device,
                                              VkCommandPool commandPool);
void endSingleShotCommandsHelper(VkDevice device,
                                 VkQueue queue,
                                 VkCommandPool commandPool,
                                 VkCommandBuffer commandBuffer);

void transitionImageLayoutHelper(VkDevice device,
                                 VkQueue queue,
                                 VkCommandPool commandPool,
                                 VkImage image,
                                 VkFormat format,
                                 VkImageLayout oldLayout,
                                 VkImageLayout newLayout);

void createBuffer(VkPhysicalDevice physicalDevice, VkDevice device,
                  VkDeviceSize size,
                  VkBufferUsageFlags usage,
                  VkMemoryPropertyFlags properties,
                  VkBuffer &buffer,
                  VkDeviceMemory &bufferMemory);

#endif // VULKANUTILS_H
