#ifndef ABSTRACTSCENE_H
#define ABSTRACTSCENE_H

#include <QObject>

#include "vulkanwindow.h"

#include <vulkan/vulkan.h>

class AbstractScene : public QObject
{
    Q_OBJECT
public:
    explicit AbstractScene(QObject *parent = nullptr);

    virtual void initialize() = 0;
    virtual void cleanup() = 0;
    virtual void cleanupSwapChain() = 0;
    virtual void resize(int w, int h) = 0;
    virtual void render() = 0;

    void setWindow(VulkanWindow *window) { m_window = window; }
    const VulkanWindow *window() const { return m_window; }
    VulkanWindow *window() { return m_window; }


protected:
    VkPhysicalDevice physicalDevice() const { return m_window->physicalDevice(); }
    VkDevice device() const { return m_window->device(); }
    VkExtent2D swapChainExtent() const { return m_window->swapChainExtent(); }
    size_t swapChainImageCount() const { return m_window->swapChainImageCount(); }
    VkQueue graphicsQueue() const { return m_window->graphicsQueue(); }

private:
    VulkanWindow *m_window;
};

#endif // ABSTRACTSCENE_H
