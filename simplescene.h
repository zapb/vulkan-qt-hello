#ifndef SIMPLESCENE_H
#define SIMPLESCENE_H

#include "abstractscene.h"
#include "vertex.h"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>

#include <vulkan/vulkan.h>

struct UniformBufferObject
{
    glm::mat4 model;
    glm::mat4 view;
    glm::mat4 proj;
};

class SimpleScene : public AbstractScene
{
public:
    SimpleScene(QObject *parent = nullptr);

protected:
    void initialize() override;
    void cleanup() override;
    void cleanupSwapChain() override;
    void resize(int w, int h) override;
    void render() override;

    void updateUniformBuffer(uint32_t currentImage);

private:
    void createDescriptorSetLayout();
    void createGraphicsPipeline();
    VkShaderModule createShaderModule(const std::vector<char> &code);
    void createCommandPool();

    void createTextureImage();
    void createTextureImageView();
    void createTextureSampler();
    void transitionImageLayout(VkImage image, VkFormat format,
                               VkImageLayout oldLayout, VkImageLayout newLayout);
    void copyBufferToImage(VkBuffer buffer, VkImage image,
                           uint32_t width, uint32_t height);
    void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size);
    void loadModel();
    void createVertexBuffer();
    void createIndexBuffer();
    void createUniformBuffers();
    void createDescriptorPool();
    void createDescriptorSets();
    void createCommandBuffers();

    VkCommandBuffer beginSingleShotCommands()
    {
        return beginSingleShotCommandsHelper(device(), m_commandPool);
    }

    void endSingleShotCommands(VkCommandBuffer commandBuffer)
    {
        endSingleShotCommandsHelper(device(), graphicsQueue(), m_commandPool, commandBuffer);
    }

    // Rendering resources
    VkDescriptorSetLayout m_descriptorSetLayout;
    VkPipelineLayout m_pipelineLayout;
    VkPipeline m_graphicsPipeline;
    VkCommandPool m_commandPool;
    VkDescriptorPool m_descriptorPool;

    // Scene resources
    VkImage m_textureImage;
    VkDeviceMemory m_textureImageMemory;
    VkImageView m_textureImageView;
    VkSampler m_textureSampler;

    std::vector<Vertex> m_vertices;
    std::vector<uint32_t> m_indices;
    VkBuffer m_vertexBuffer;
    VkDeviceMemory m_vertexBufferMemory;
    VkBuffer m_indexBuffer;
    VkDeviceMemory m_indexBufferMemory;

    std::vector<VkBuffer> m_uniformBuffers;
    std::vector<VkDeviceMemory> m_uniformBuffersMemory;

    // For binding scene resources to the rendering resources and actually rendering
    std::vector<VkDescriptorSet> m_descriptorSets;
    std::vector<VkCommandBuffer> m_commandBuffers;
};

#endif // SIMPLESCENE_H
